CryptoJS = require('crypto-js')
function decryptByDES(ciphertext, key) {
			var keyHex = CryptoJS.enc.Utf8.parse(key);
			// direct decrypt ciphertext
			var decrypted = CryptoJS.DES.decrypt({
				ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
			}, keyHex, {
				mode: CryptoJS.mode.ECB,
				padding: CryptoJS.pad.Pkcs7
			});
			return decrypted.toString(CryptoJS.enc.Utf8);
	}
	function strKey(){
		var key="Ctpsp@884*"
		return key
	}

	function get_data(detail_id,ciphertext){
		var data=JSON.parse(decryptByDES(ciphertext,strKey()))
		var detail_pdf_url="https://details.cebpubservice.com:7443/bulletin/getBulletin/"+data.data+"/"+detail_id
		var pdfUrl="https://bulletin.cebpubservice.com/resource/ceb/js/pdfjs-dist/web/viewer.html?file="+detail_pdf_url
		return pdfUrl
	}
