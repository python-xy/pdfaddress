import requests
from lxml import etree
import execjs


def get_pdfurl(res_url):
    key_url = 'https://details.cebpubservice.com:7443/permission/getSecretKey'
    headers = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36'
    }
    res_res = requests.get(res_url,headers=headers)
    html = etree.HTML(res_res.text)
    detail_id = html.xpath('//div[@class="mian_list_03"]/@index')[0]
    ciphertext = requests.post(key_url,headers=headers).text

    data = {
        'detail_id':detail_id,
        'ciphertext':ciphertext[1:-1]
    }
    with open('./main.js','r') as fp:
        js_data = fp.read()

    res = execjs.compile(js_data).call('get_data',data['detail_id'],data['ciphertext'])
    return res

if __name__ == '__main__':
    res_url = 'https://bulletin.cebpubservice.com/biddingBulletin/2023-03-21/9406787.html'
    pdf_url = get_pdfurl(res_url)
    print(pdf_url)
